import { render } from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { App, Details, Home, Profile } from "./pages";
import "./styles/variables.css";
import "./styles/global.css";

const rootElement = document.getElementById("root");
const renderApp = () =>
  render(
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<Home />} />
          <Route path="details" element={<Details />} />
          <Route path="details/:id" element={<Details />} />
          <Route path="Profile" element={<Profile />} />
        </Route>
      </Routes>
    </BrowserRouter>,
    rootElement
  );

// TODO: fix so only the main content gets rerendered.
if (process.env.NODE_ENV === "development") {
  import("./mocks/browser").then(({ worker }) => {
    worker.start();
    renderApp();
  });
} else {
  renderApp();
}
