// credit: https://usehooks-ts.com/react-hook/use-fetch
import { useEffect, useReducer, useRef } from "react";

interface State<T> {
  data?: T;
  error?: Error;
}

type Cache<T> = { [url: string]: T };

// discriminated union type
type Action<T> =
  | { type: "loading" }
  | { type: "fetched"; payload: T }
  | { type: "error"; payload: Error };

export function useData<T = unknown>(service: () => Promise<T>): State<T> {
  const cache = useRef<Cache<T>>({});

  // Used to prevent state update if the component is unmounted
  const cancelRequest = useRef<boolean>(false);

  const initialState: State<T> = {
    error: undefined,
    data: undefined,
  };

  // Keep state logic separated
  const fetchReducer = (state: State<T>, action: Action<T>): State<T> => {
    switch (action.type) {
      case "loading":
        return { ...initialState };
      case "fetched":
        return { ...initialState, data: action.payload };
      case "error":
        return { ...initialState, error: action.payload };
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(fetchReducer, initialState);

  useEffect(() => {
    // Do nothing if the callback isn't given,
    if (!service) return;

    const callService = async () => {
      dispatch({ type: "loading" });

      if (cache.current[service.name]) {
        dispatch({ type: "fetched", payload: cache.current[service.name] });
        return;
      }

      service()
        .then((data) => {
          cache.current[service.name] = data;
          if (cancelRequest.current) return;

          dispatch({ type: "fetched", payload: data });
        })
        .catch(() => {
          if (cancelRequest.current) return;
        });
    };
    void callService();
    // Use the cleanup function for avoiding a possibly...
    // ...state update after the component was unmounted
    return () => {
      cancelRequest.current = true;
    };
  }, []);

  return state;
}
