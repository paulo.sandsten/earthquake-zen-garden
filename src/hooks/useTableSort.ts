import { useReducer, useState } from "react";

export type SortTableAction = {
  direction: SortTableDirection;
  payload: { [key: string]: any }[];
  column: string;
};

export enum SortTableDirection {
  INIT = "init",
  ASCENDING = "ascending",
  DESCENDING = "descending",
}

type SortMetaData = {
  direction: SortTableDirection;
  column: string;
};

export function useTableSort<T>(initialState = [] as T[]) {
  const [sortMetaData, setSortMetaData] = useState<SortMetaData>({
    direction: SortTableDirection.INIT,
    column: "",
  });

  const getSortOrder = (column: string) =>
    !column
      ? SortTableDirection.INIT
      : sortMetaData.column !== column
      ? SortTableDirection.ASCENDING
      : sortMetaData.direction === SortTableDirection.ASCENDING
      ? SortTableDirection.DESCENDING
      : SortTableDirection.ASCENDING;

  const sortColumn = (action: SortTableAction) => {
    const { column, payload, direction } = action;
    return [...payload].sort((a, b) => {
      if (a[column] < b[column])
        return direction === SortTableDirection.ASCENDING ? -1 : 1;
      if (a[column] > b[column])
        return direction === SortTableDirection.ASCENDING ? 1 : -1;
      return 0;
    });
  };

  const reducer = <T>(state: T[], action: SortTableAction) => {
    const { direction, payload } = action;
    switch (direction) {
      case SortTableDirection.ASCENDING:
        return [...sortColumn(action)];
      case SortTableDirection.DESCENDING:
        return [...sortColumn(action)];
      case SortTableDirection.INIT:
        return [...payload];
      default:
        return state;
    }
  };

  const sortTable = (state: T[], column: string = "") => {
    const direction = getSortOrder(column);
    setSortMetaData({ direction, column });
    dispatch({ direction, column, payload: [...state] });
  };

  const [state, dispatch] = useReducer(reducer, initialState);
  return { state, sortTable };
}
