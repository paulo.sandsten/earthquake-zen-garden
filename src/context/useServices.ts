import { createContext, useContext } from "react";
import { DI } from "../services";

export const ServicesContext = createContext<DI>(DI.getInstance());
export const useServices = (): DI => useContext(ServicesContext);
