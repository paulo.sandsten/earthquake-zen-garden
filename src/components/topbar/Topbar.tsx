import { Link } from "react-router-dom";
import styles from "./Topbar.module.css";

interface P {
  siteTitle: string;
  logoImage: string;
  userName: string;
}
export const Topbar: React.FC<P> = ({ siteTitle, logoImage, userName }) => {
  return (
    <header className={styles.topbar}>
      <div className={styles.heroWrapper}>
        <Link to="/">
          <img src={logoImage} alt="logo" />
        </Link>
      </div>
      <h1 className={styles.siteTitle}>{siteTitle}</h1>
      <Link to="/profile" className={styles.profileLink}>
        Welcome {userName}
      </Link>
    </header>
  );
};
