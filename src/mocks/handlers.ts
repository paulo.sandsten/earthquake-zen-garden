import { rest } from "msw";
import earthquakesSummary from "./data/earthquakes.json";
import profile from "./data/profile.json";

export const handlers = [
  rest.get("/api/user/-", (_req, res, ctx) => {
    return res(ctx.json(profile));
  }),
  rest.get(
    "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_hour.geojson",
    (_req, res, ctx) => {
      return res(ctx.json(earthquakesSummary));
    }
  ),
];
