import { DI, HttpClient, Logger } from ".";
import { Earthquake, EarthquakesSummary } from "../types";

export type EarthquakeModel = {
  id: string;
  title: string;
  magnitude: number;
  time: string;
  status: string;
  tsunami: number;
  type: string;
};

export type EarthquakesModel = {
  title: string;
  earthquakes: EarthquakeModel[];
};

// Not all services need to be Singletons. I wanted the cache here to stay, so I chose a Singleton
// as we have two pages that are using the data from the earthquakes array.
export class EarthquakeService {
  private static instance: EarthquakeService;
  private constructor(private httpClient: HttpClient, private logger: Logger) {}
  private viewModel: EarthquakesModel | undefined;
  private getAllUrl =
    "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_hour.geojson";

  public static getInstance(
    httpClient: HttpClient,
    logger: Logger
  ): EarthquakeService {
    if (!EarthquakeService.instance) {
      EarthquakeService.instance = new EarthquakeService(httpClient, logger);
    }

    return EarthquakeService.instance;
  }

  private convertDateTime = (ms: number) =>
    new Date(ms).toLocaleString("en-US", {
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    });

  private createViewModel = (
    earthquakesSummary: EarthquakesSummary
  ): EarthquakesModel => {
    const title = earthquakesSummary.data.metadata.title;
    const earthquakes = earthquakesSummary.data.features.map(
      (earthquake: Earthquake): EarthquakeModel => ({
        id: earthquake.id,
        title: earthquake.properties.place,
        magnitude: earthquake.properties.mag,
        time: this.convertDateTime(earthquake.properties.time),
        status: earthquake.properties.status,
        tsunami: earthquake.properties.tsunami,
        type: earthquake.properties.type,
      })
    );

    return { title, earthquakes };
  };

  getViewModel = () => {
    return this.viewModel
      ? Promise.resolve(this.viewModel)
      : this.httpClient
          .get<EarthquakesSummary>(this.getAllUrl)
          .then((response: EarthquakesSummary) => {
            this.logger.log(`Fetched earthquake summary.`);
            this.viewModel = this.createViewModel(response);
            return this.viewModel;
          });
  };
}
