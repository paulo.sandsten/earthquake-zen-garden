import { EarthquakeService, UserService, HttpClient, Logger } from ".";

export class DI {
  private static instance: DI;
  public logger: Logger;
  public httpClient: HttpClient;
  public earthquakeService: EarthquakeService;
  public userService: UserService;

  private constructor() {
    this.logger = Logger.getInstance();
    this.httpClient = HttpClient.getInstance(this.logger);
    this.earthquakeService = EarthquakeService.getInstance(
      this.httpClient,
      this.logger
    );
    this.userService = UserService.getInstance(this.httpClient, this.logger);
  }

  public static getInstance(): DI {
    if (!DI.instance) {
      DI.instance = new DI();
    }

    return DI.instance;
  }
}
