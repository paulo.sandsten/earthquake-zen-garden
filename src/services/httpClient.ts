import { Logger } from ".";

// This is a fast error util function, in production I would personally use
// custom errors that live in their own common directory.
class ClientException {
  public name: string;
  public status: number;

  constructor(status: number) {
    this.name = "ClientException";
    this.status = status;
  }
}

export class HttpClient {
  private static instance: HttpClient;
  private constructor(private logger: Logger) {}

  public static getInstance(logger: Logger): HttpClient {
    if (!HttpClient.instance) {
      HttpClient.instance = new HttpClient(logger);
    }

    return HttpClient.instance;
  }

  private http<T>(url: string, options = {}): Promise<T> {
    return fetch(url, {
      headers: {
        "Content-Type": "application/json",
      },
      ...options,
    })
      .then((res) => {
        if (res.status >= 400) {
          throw new ClientException(res.status);
        }
        return res;
      })
      .then((res) => res.json())
      .catch((error) => {
        this.logger.error(
          `Backend returned code ${error.status}, body was: `,
          error.error
        );
      });
  }

  get<T>(url: string): Promise<T> {
    return this.http(url, {
      method: "GET",
    });
  }
}
