// Quick util for logging. In Prod, we would use other logging tools.
export class Logger {
  private static instance: Logger;
  private constructor() {}

  public static getInstance(): Logger {
    if (!Logger.instance) {
      Logger.instance = new Logger();
    }

    return Logger.instance;
  }

  log<T>(...args: T[]) {
    console.log(...args);
  }

  error<T>(...args: T[]) {
    console.error(...args);
  }
}
