import { HttpClient, Logger } from ".";
import { User } from "../types";

export class UserService {
  private static instance: UserService;
  private constructor(private httpClient: HttpClient, private logger: Logger) {}
  private user: User | undefined;
  private getUserUrl = "/api/user/-";

  public static getInstance(
    httpClient: HttpClient,
    logger: Logger
  ): UserService {
    if (!UserService.instance) {
      UserService.instance = new UserService(httpClient, logger);
    }

    return UserService.instance;
  }

  getUser = () => {
    return this.user
      ? Promise.resolve(this.user)
      : this.httpClient.get<User>(this.getUserUrl).then((user: User) => {
          this.logger.log(`Fetched user profile.`);
          this.user = { ...user };

          return this.user;
        });
  };
}
