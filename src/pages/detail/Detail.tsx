import { useParams } from "react-router-dom";
import { useServices } from "../../context/useServices";
import { useData } from "../../hooks";
import { EarthquakesModel } from "../../services";
import styles from "./Detail.module.css";

export const Details = () => {
  const { id } = useParams();
  const { earthquakeService } = useServices();
  const { data, error } = useData<EarthquakesModel>(
    earthquakeService.getViewModel
  );

  if (error) return <p>There is an error.</p>;
  if (!data) return <p>Loading...</p>;

  const detail = data.earthquakes.find((e) => e.id === id);
  return (
    <>
      {detail && (
        <div className={styles.wrapper}>
          <h2 className={`${styles.title} title`}>{detail.title}</h2>
          <div className={styles.grid}>
            <p>Title</p>
            <p>{detail.title}</p>
            <p>Magnitude</p>
            <p>{detail.magnitude}</p>
            <p>Time</p>
            <p>{detail.time}</p>
            <p>Status</p>
            <p>{detail.status}</p>
            <p>Tsunami</p>
            <p>{detail.tsunami}</p>
            <p>Type</p>
            <p>{detail.type}</p>
          </div>
        </div>
      )}
    </>
  );
};
