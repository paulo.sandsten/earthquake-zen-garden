import { useServices } from "../../context/useServices";
import { useData } from "../../hooks";
import { User } from "../../types";
import styles from "./Profile.module.css";

export const Profile = () => {
  const { userService } = useServices();
  const { data, error } = useData<User>(userService.getUser);

  if (error) return <p>There is an error.</p>;
  if (!data) return <p>Loading...</p>;

  return (
    <>
      <h2 className="title">Profile</h2>
      <div className={styles.grid}>
        <div className={styles.imageWrapper}>
          <img src={data.profile.avatarImage} alt="profile" />
        </div>
        <div className={styles.bioGrid}>
          <p>First name</p>
          <p>{data.profile.firstName}</p>
          <p>Last name</p>
          <p>{data.profile.lastName}</p>
          <p>Phone</p>
          <p>{data.profile.phone}</p>
          <p>Email</p>
          <p>{data.profile.email}</p>
          <p>Bio</p>
          <p>{data.profile.bio}</p>
        </div>
      </div>
    </>
  );
};
