import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useServices } from "../../context/useServices";
import { useData, useTableSort } from "../../hooks";
import { EarthquakesModel, EarthquakeModel } from "../../services";
import styles from "./Home.module.css";

enum Column {
  TITLE = "title",
  MAGNITUDE = "magnitude",
  TIME = "time",
}

export const Home = () => {
  const { earthquakeService } = useServices();
  const { data, error } = useData<EarthquakesModel>(
    earthquakeService.getViewModel
  );
  const { state, sortTable } = useTableSort();

  useEffect(() => {
    if (data) sortTable(data.earthquakes);
  }, [data]);

  if (error) return <p>There is an error.</p>;
  if (!data || !state) return <p>Loading...</p>;

  return (
    <>
      <h2 className="title">{data.title}</h2>
      <table className={styles.table}>
        <thead>
          <tr>
            <th onClick={() => sortTable(state, Column.TITLE)}>Title</th>
            <th onClick={() => sortTable(state, Column.MAGNITUDE)}>
              Magnitude
            </th>
            <th onClick={() => sortTable(state, Column.TIME)}>Time</th>
          </tr>
        </thead>
        <tbody>
          {(state as EarthquakeModel[]).map(
            (earthquake: EarthquakeModel, i: number) => (
              <tr key={i}>
                <td>
                  <Link
                    className={styles.link}
                    to={`/details/${earthquake.id}`}
                  >
                    {earthquake.title}
                  </Link>
                </td>
                <td>{earthquake.magnitude}</td>
                <td>{earthquake.time}</td>
              </tr>
            )
          )}
        </tbody>
      </table>
    </>
  );
};
