import { Outlet } from "react-router-dom";
import { useServices } from "../context/useServices";
import { useData } from "../hooks";
import { Topbar } from "../components";
import { User } from "../types";
import siteData from "../config/site.json";

export const App = () => {
  const { userService } = useServices();
  const { data, error } = useData<User>(userService.getUser);

  if (error) return <p>There is an error.</p>;
  if (!data) return <p>Loading...</p>;

  return (
    <>
      <Topbar
        siteTitle={siteData.site.title}
        logoImage={siteData.site.logoImage}
        userName={data.profile.firstName}
      />
      <main className="container">
        <Outlet />
      </main>
    </>
  );
};
