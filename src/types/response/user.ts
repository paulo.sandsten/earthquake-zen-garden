export type Profile = {
  firstName: string;
  lastName: string;
  avatarImage: string;
  phone: string;
  email: string;
  bio: string;
};

export type User = {
  profile: Profile;
};
