export type EarthquakeProperties = {
  mag: number;
  place: string;
  time: number;
  updated: number;
  tz: number;
  url: string;
  detail: string;
  felt: null;
  cdi: null;
  mmi: null;
  alert: null;
  status: string;
  tsunami: number;
  sig: number;
  net: string;
  code: string;
  ids: string;
  sources: string;
  types: string;
  nst: number;
  dmin: number;
  rms: number;
  gap: number;
  magType: string;
  type: string;
  title: string;
};

export type EarthquakeGeometry = {
  type: string;
  coordinates: number[];
};

export type Earthquake = {
  type: "Feature";
  properties: EarthquakeProperties;
  geometry: EarthquakeGeometry;
  id: string;
};

export type Metadata = {
  api: number;
  count: number;
  generated: number;
  status: number;
  title: string;
  url: string;
};

export type EarthquakesSummaryData = {
  bbox: number[];
  features: Earthquake[];
  metadata: Metadata;
  type: string;
};

export type EarthquakesSummary = {
  data: EarthquakesSummaryData;
};
